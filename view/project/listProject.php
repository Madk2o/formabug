<?php 
    require "./da/project.php";

    $pseudo = $_SESSION["login_connected"];
    $id_role = $_SESSION["id_role_connected"];
    $listProject = displayProject();
    $testInnerJoin = testInnerJoin($pseudo);
    // echo "<pre>";
    // var_dump($testInnerJoin);
    // echo "</pre>";
?>

<table class="table box is-flex is-flex-direction-column	">
  <tbody>
    <tr class="is-flex is-flex-direction-row">
      <th class="column is-3"><abbr title="Position">Id</abbr></th>
      <th class="column is-5">Titre</th>
      <th class="column is-2"></th>
      <th class="column is-2"></th>
    </tr>
    <?php 
    if(isset($_SESSION["id_role_connected"]) && ($_SESSION["id_role_connected"] == 1 || $_SESSION["id_role_connected"] == 4)):
    foreach ($listProject as $value): ?>
    <tr class="is-flex is-flex-direction-row">
        <th class="column is-3"><?php echo $value->id_project?></th>
        <td class="column is-5"><?php echo $value->titre?></td>
        <?php if(isset($_SESSION["id_role_connected"]) && ($_SESSION["id_role_connected"] == 1 || $_SESSION["id_role_connected"] == 4)): ?>
        <td class="column is-2"><a class="button is-danger" href="<?php echo "index.php?pageProject=deleteProject&id=".$value->id_project?>">Delete</a></td>
        <?php endif; ?>  
    </tr>
    <?php endforeach; ?>
    <?php endif; ?>
    
    <?php
    if(isset($_SESSION["id_role_connected"]) && ($_SESSION["id_role_connected"] != 1 && $_SESSION["id_role_connected"] != 4)): ?>
    <?php foreach ($testInnerJoin as $value): ?>
    <tr class="is-flex is-flex-direction-row">
        <th class="column is-3"><?php echo $value->id_project?></th>
        <td class="column is-5"><?php echo $value->titre?></td>
    </tr>
    <?php endforeach; ?>
    <?php endif; ?>
  </tbody>
</table>