<?php
require "./da/project.php"
?>
<form class="box is-flex is-flex-direction-column" action="bu/insertProject.php" method="post">
    <h2 class="is-size-1-mobile">Creation du projet :</h2>
    <div class="is-flex is-flex-wrap-wrap">
        <div class="column is-6">
            <div class="field is-flex is-flex-wrap-wrap is-align-items-center">
              <label class="label form_name">Titre du projet :</label>
              <div class="control">
                <input class="input" type="text" name="nom" placeholder="">
              </div>
            </div>
            
            <div class="field is-flex is-flex-wrap-wrap is-align-items-center">
              <label class="label form_name">Client :</label>
              <div class="control">
                <div class="select">
                  <select name="client">
                  <?php foreach(getClientProject() as $value): ?>
                    <option value="<?php echo $value->id_users ?>"><?php echo $value->pseudo ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>
            </div>
            
            <div class="field is-flex is-flex-wrap-wrap is-align-items-center">
              <label class="label form_name">Developpeurs assigné :</label>
              <div class="control">
                <div class="select is-multiple">
                  <select multiple name="dev[]">
                  <?php foreach(getDevProject() as $value): ?>
                    <option value="<?php echo $value->id_users ?>"><?php echo $value->pseudo ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>
            </div>
        </div>
    </div>
    
    <div class="mt-1 field is-flex-wrap-wrap is-align-items-center">
      <label class="label">Description du projet :</label>
      <div class="control">
        <textarea class="textarea" name="messagejoint" placeholder="Entrez votre message..."></textarea>
      </div>
    </div>
    
    <div class="field is-grouped is-flex is-flex-wrap-wrap">
      <div class="control">
        <button class="button is-link">Submit</button>
      </div>
      <div class="control">
        <button class="button is-link is-light">Cancel</button>
      </div>
    </div>
</form>