<form class="box" action="../../bu/verifLogin.php" method="post">
    <h2 class="is-size-4 mb-3">Se connecter :</h2>
    <div class="field">
        <p class="control has-icons-left has-icons-right">
            <input class="input" type="text" placeholder="Pseudo" name="loginpseudo">
            <span class="icon is-small is-left">
            <i class="fa fa-envelope"></i>
            </span>
            <span class="icon is-small is-right">
            <i class="fa fa-check"></i>
            </span>
        </p>
    </div>
    <div class="field">
        <p class="control has-icons-left">
            <input class="input" type="password" placeholder="Password" name="loginpassword">
            <span class="icon is-small is-left">
            <i class="fa fa-lock"></i>
            </span>
        </p>
    </div>
    <p class="field ml-1">Pas de compte utilisateur? Contactez-nous <a href="#">info@jeanvaljean.be</a></p>
    <div class="field">
        <p class="control">
            <button class="button is-success">
            Login
            </button>
        </p>
    </div>
</form>