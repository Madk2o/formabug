<?php 

  $_SESSION["id"] = null;
  $_SESSION["role"] = null;
  if(!isset($_SESSION["error"])){
    $_SESSION["error"] = 0;
  }

?>
<form class="box is-flex is-flex-direction-column" action="../bu/verifRole.php" method="post">
<?php 
  if($_SESSION["error"] === 1){
    $_SESSION["error"] = 0;
    foreach($_SESSION["message"] as $message){
      echo $message. "<br>";
  }
    $_SESSION["message"] = null;
  }
?>
    <h2 class="is-size-1-mobile">Creation de role :</h2>
    <div class="is-flex is-flex-wrap-wrap">
        <div class="column is-6">
            <div class="field is-flex is-flex-wrap-wrap is-align-items-center">
              <label class="label form_name">Intitulé :</label>
              <div class="control">
                <input class="input" type="text" name="role" placeholder="">
              </div>
            </div>

            <div class="field is-grouped is-flex is-flex-wrap-wrap">
              <div class="control">
                <button class="button is-link">Submit</button>
              </div>
              <div class="control">
                <button class="button is-link is-light">Cancel</button>
              </div>
            </div>
        </div>
    </div>
</form>