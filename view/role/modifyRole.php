<?php
require "./da/roles.php";

if(!isset($_SESSION["error"])){
    $_SESSION["error"] = 0;
  }

if(isset($_GET["id"])){
    $result = editRole($_GET["id"]);
    $_SESSION["id"] = $id = $result->id_role;
    $_SESSION["role"] = $role = $result->role;
    
}

?>

<form class="box" action="../../bu/verifRole.php" method="post">
    <?php 
    if($_SESSION["error"] === 1){
        $_SESSION["error"] = 0;
        foreach($_SESSION["message"] as $message){
        echo $message. "<br>";
    }
        $_SESSION["message"] = null;
    }
    ?>
    <h2 class="is-size-4 mb-3">Modification de role :</h2>
    <div class="field">
        <p class="control has-icons-left has-icons-right">
            <input class="input" type="text" name="role" value="<?php echo $role ?>">
        </p>
    </div>
    <div class="field">
        <p class="control has-icons-left">
            <!-- <input class="input" type="password" placeholder="Password" name="loginpassword"> -->
        </p>
    </div>
    <div class="field">
        <p class="control">
            <button class="button is-success">
            Apply
            </button>
        </p>
    </div>
</form>