<?php 
    require "./da/roles.php";

    $listRole = displayRole();
?>

<table class="table box is-flex is-flex-direction-column	">
  <tbody>
    <tr class="is-flex is-flex-direction-row">
      <th class="column is-1"><abbr title="Position">ID</abbr></th>
      <th class="column is-3">Rôle</th>
      <th class="column is-3"></th>
      <th class="column is-3"></th>
    </tr>
    <?php foreach ($listRole as $value): ?>
    <tr class="is-flex is-flex-direction-row">
        <th class="column is-1"><?php echo $value->id_role?></th>
        <td class="column is-3"><?php echo $value->role?></td>
        <td class="column is-3"><a class="button is-warning" href="<?php echo "index.php?pageRole=modifyRole&id=".$value->id_role ?>">Modify</a></td>
        <td class="column is-3"><a class="button is-danger" href="<?php echo "index.php?pageRole=deleteRole&id=".$value->id_role ?>">Delete</a></td>  
    </tr>
    <?php endforeach ?>
  </tbody>
</table>
