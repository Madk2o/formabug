<?php
    require "da/dbaccess.php";
    $id = $_GET["id"];

try{
    $bdd = bdd();
    
    $requete = $bdd->prepare("DELETE FROM type_roles WHERE id_role = ?");
    $requete->execute([$id]);

    header('Location: ../index.php?pageRole=listRole');
    exit();

}catch(PDOException $e){

    echo $e->getMessage();
    echo $e->getLine();
    exit;
}