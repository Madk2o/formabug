<?php
require __DIR__."/../../da/dbaccess.php";

if(!isset($_SESSION["pseudo"]) && !isset($_SESSION["email"]) && !isset($_SESSION["password"])){
  $_SESSION["pseudo"] = null;
  $_SESSION["email"] = null;
  $_SESSION["password"] = null;
  }
?>
<form class="box is-flex is-flex-direction-column" action="../../bu/verifUser.php" method="post">
    <?php 
    if(isset($_SESSION["error"]) && $_SESSION["error"] === 1){
        $_SESSION["error"] = 0;
        foreach($_SESSION["message"] as $message){
        echo $message. "<br>";
    }
        $_SESSION["message"] = null;
    }
    ?>
    <h2 class="is-size-1-mobile">Creation d'un utilisateur :</h2>
    <div class="is-flex is-flex-wrap-wrap">
        <div class="column is-12">
            <div class="field is-flex is-flex-wrap-wrap is-align-items-center">
              <label class="label form_name">Pseudo :</label>
              <div class="control">
                <input class="input" type="text" name="pseudo" value="<?php echo $_SESSION["pseudo"] ?>" >
              </div>
            </div>
            
            <div class="field is-flex is-flex-wrap-wrap is-align-items-center">
              <label class="label form_name">Email :</label>
              <div class="control">
                <input class="input" type="email" name="email" value="<?php echo $_SESSION["email"] ?>">
              </div>
            </div>

            <div class="field is-flex is-flex-wrap-wrap is-align-items-center">
              <label class="label form_name">Mot de passe :</label>
              <div class="control">
                <input class="input" type="password" name="password" value="<?php echo $_SESSION["password"] ?>">
              </div>
            </div>
            <div class="field is-flex is-flex-wrap-wrap is-align-items-center">
              <label class="label form_name">Role :</label>
              <div class="control">
                <div class="select">
                  <select name="registerRole">
                    <?php foreach(getRoles() as $value): ?>
                    <option value="<?php echo $value->id_role ?>"><?php echo $value->role ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>
              <div class="field is-grouped is-flex is-flex-wrap-wrap">
            <div class="control">
                <button class="button is-link">Submit</button>
            </div>
            <div class="control">
                <button class="button is-link is-light">Cancel</button>
            </div>
        </div>
            </div>
        </div>
    </div>
</form>
<?php 
        $_SESSION["pseudo"] = null;
        $_SESSION["email"] = null;
        $_SESSION["password"] = null;
        $_SESSION["registerRole"] = null;
        $_SESSION["id"] = null;
?>