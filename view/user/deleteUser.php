<?php
    require "da/dbaccess.php";
    $id = $_GET["id"];

try{
    $bdd = bdd();
    
    $requete = $bdd->prepare("DELETE FROM users WHERE id_users = ?");
    $requete->execute([$id]);

    header('Location: ../index.php?pageUser=listUser');
    exit();

}catch(PDOException $e){

    echo $e->getMessage();
    echo $e->getLine();
    exit;
}