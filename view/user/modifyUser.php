<?php

require "./da/users.php";

if(!isset($_SESSION["error"])){
    $_SESSION["error"] = 0;
  }

if(isset($_GET["id"])){
    $result = editUser($_GET["id"]);
    $_SESSION["id"] = $id = $result->id_users;
    $_SESSION["pseudo"] = $pseudo = $result->pseudo;
    $_SESSION["email"] = $email = $result->email;
    $_SESSION["pseudo"] = $registerRole = $result->pseudo;
}



?>
<form class="box" action="../../bu/verifUser.php" method="post">
    <?php 
    if($_SESSION["error"] === 1){
        $_SESSION["error"] = 0;
        foreach($_SESSION["message"] as $message){
        echo $message. "<br>";
    }
        $_SESSION["message"] = null;
    }
    ?>
    <h2 class="is-size-1-mobile">Creation d'un utilisateur :</h2>
    <div class="is-flex is-flex-wrap-wrap">
        <div class="column is-12">
            <div class="field is-flex is-flex-wrap-wrap is-align-items-center">
              <label class="label form_name">Pseudo :</label>
              <div class="control">
                <input class="input" type="text" name="pseudo" value="<?php echo $_SESSION["pseudo"] ?>" >
              </div>
            </div>
            
            <div class="field is-flex is-flex-wrap-wrap is-align-items-center">
              <label class="label form_name">Email :</label>
              <div class="control">
                <input class="input" type="email" name="email" value="<?php echo $_SESSION["email"] ?>">
              </div>
            </div>

            <div class="field is-flex is-flex-wrap-wrap is-align-items-center">
              <label class="label form_name">Mot de passe :</label>
              <div class="control">
                <input class="input" type="password" name="password" value="">
              </div>
            </div>

            <div class="field is-flex is-flex-wrap-wrap is-align-items-center">
              <label class="label form_name">Role :</label>
              <div class="control">
                <div class="select">
                  <select name="registerRole">
                    <?php foreach(getRoles() as $value): ?>
                    <option value="<?php echo $value->id_role ?>"><?php echo $value->role ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>
            </div>
        </div>

        <div class="field is-grouped is-flex is-flex-wrap-wrap">
            <div class="control">
                <button class="button is-link">Submit</button>
            </div>
            <div class="control">
                <button class="button is-link is-light">Cancel</button>
            </div>
        </div>
    </div>
</form>
<?php 
        $_SESSION["pseudo"] = null;
        $_SESSION["email"] = null;
        $_SESSION["password"] = null;
        $_SESSION["register_role"] = null;
?>