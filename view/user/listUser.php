<?php 
    require "./da/users.php";

    $listUser = displayUser();
?>

<table class="table box is-flex is-flex-direction-column	">
  <tbody>
    <tr class="is-flex is-flex-direction-row">
      <th class="column is-3"><abbr title="Position">Pseudo</abbr></th>
      <th class="column is-3">Email</th>
      <th class="column is-2">Rôle</th>
      <th class="column is-2"></th>
      <th class="column is-2"></th>
    </tr>
    <?php foreach ($listUser as $value): ?>
    <tr class="is-flex is-flex-direction-row">
        <th class="column is-3"><?php echo $value->pseudo?></th>
        <td class="column is-3"><?php echo $value->email?></td>
        <td class="column is-2"><?php echo $value->id_role?></td>
        

        <td class="column is-2"><a class="button is-warning" href="<?php echo "index.php?pageUser=modifyUser&id=".$value->id_users?>"><i class="fas fa-pen-nib"></i> Modify</a></td>
        <td class="column is-2"><a class="button is-danger" href="<?php echo "index.php?pageUser=deleteUser&id=".$value->id_users?>"><i class="fas fa-trash-alt"></i> Delete</a></td>  
    </tr>
    <?php endforeach ?>
  </tbody>
</table>