<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FormaBug</title>
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <!-- Bulma Version 0.9.0-->
    <link rel="stylesheet" href="../asset/css/bulma.min.css"/>
    <link rel="stylesheet" type="text/css" href="../asset/css/admin.css">
    <link rel="stylesheet" href="../asset/css/style.css">
</head>
<body>
    <nav class="navbar is-danger">
        <div class="container">
            <div class="navbar-brand">
                <a class="navbar-item brand-text" href="index.php">
                FormaBug
                </a>
                <div class="navbar-burger burger" data-target="navMenu">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div id="navMenu" class="navbar-menu">

                <div class="navbar-end">
                    <?php 
                    if($_SESSION["connected"]): ?>
                        <div class="navbar-item dropdown">
                            <div class="dropdown-trigger">
                                <p class="box">
                                <span><?php echo $_SESSION["login_connected"] ?></span>
                                </p>
                            </div>
                        </div>    
                        <a class="navbar-item" href="./view/login/disconnect.php">
                            Se déconnecter
                        </a>
                    <?php endif; ?>
                    <?php if($_SESSION["connected"] === null): ?>
                        <a class="navbar-item" href="index.php?pageLogin=login">
                            Se connecter
                        </a>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="columns">
            <div class="column is-2 ">
                <aside class="menu is-hidden-mobile">
                    <p class="menu-label">
                        General
                    </p>
                    <ul class="menu-list">
                        <li><a href="index.php?pageTicket=newTicket">Nouveau ticket</a></li>
                        
                    </ul>
                </aside>
                <?php
                if(isset($_SESSION["id_role_connected"]) && $_SESSION["id_role_connected"] == 1 ): ?>
                <aside class="menu is-hidden-mobile">
                    <p class="menu-label">
                        Administration
                    </p>
                    <ul class="menu-list">
                        <li><a href="index.php?pageRole=insertRole">Nouveau rôle</a></li>
                        <li><a href="index.php?pageRole=listRole">Liste des rôles</a></li>
                        <li><a href="index.php?pageUser=listUser">Liste des Utilisateurs</a></li>
                        <li><a href="index.php?pageUser=createUser">Creer un nouvel utilisateur</a></li>
                        <li><a href="index.php?pageProject=listProject">Liste des projets</a></li>
                        <li><a href="index.php?pageProject=newProject">Nouveau projet</a></li>
                    </ul>
                </aside>
                <?php endif ?>   
            </div>
            <div class="column is-10">
                <section class="hero is-danger welcome is-small">
                    <div class="hero-body">
                        <div class="container">
                        <?php if(isset($_SESSION["id_role_connected"]) && $_SESSION["id_role_connected"] == 1): ?>
                            <h1 class="title">
                                Hello, Boss.
                            </h1>
                        <?php endif; ?>
                        <?php if(isset($_SESSION["id_role_connected"]) && $_SESSION["id_role_connected"] == 2): ?>
                            <h1 class="title">
                                Hello, €$€$.
                            </h1>
                        <?php endif; ?>
                        <?php if(isset($_SESSION["id_role_connected"]) && $_SESSION["id_role_connected"] == 4): ?>
                            <h1 class="title">
                                Hello, Developper.
                            </h1>
                        <?php endif; ?>
                            <h2 class="subtitle">
                                I hope you are having a great day!
                            </h2>
                        </div>
                    </div>
                </section>
                <section>
                    <?php
                        if(isset($_GET["pageRole"])){
                            include 'view/role/'. $_GET['pageRole'] .'.php';
                        }
                        elseif(isset($_GET["pageUser"])){
                            include 'view/user/'. $_GET['pageUser'] .'.php';
                        }
                        elseif(isset($_GET["pageLogin"])){
                            include 'view/login/'. $_GET['pageLogin'] .'.php';
                        }
                        elseif(isset($_GET["pageTicket"])){
                            include 'view/ticket/'. $_GET['pageTicket'] .'.php';
                        }
                        elseif(isset($_GET["pageProject"])){
                            include 'view/project/'. $_GET['pageProject'] .'.php';
                        }
                        else{
                            include "view/main.php";
                        }
                    ?>
                </section>
            </div>
        </div>
    </div>
<script async type="text/javascript" src="../asset/js/bulma.js"></script>
<script src="../asset/js/input_file.js"></script>
</body>
</html>