<?php

if ($_SESSION["connected"] == false) {
    header("location: ../index.php");
    exit();
}
require "./da/viewTicket.php";


?>
<section class="content">
    <div class="containerfluid py-5">
        <div class="container py-5">
            <div class="d-flex justify-content-center py-3">
                <a class="btn btn-info" href="index.php?pageTicket=newTicket">Ajout Ticket</a>
            </div>
            <div class="d-flex justify-content-center">

                <table>
                    <tr>
                        <th class="p-3">ID ticket</th>
                        <th class="p-3">Nom</th>
                        <th class="p-3">Prénom</th>
                        <th class="p-3">Email</th>
                        <th class="p-3">Titre du projet</th>
                        <th class="p-3">Description</th>
                    </tr>

<?php

$result = listTicket($value->$id_ticket, $nom, $prenom, $email, $titre, $description);
?>

                    <tr>     
                    <?php foreach ($result as $value) :
                        
                    ?>

                            <td class="p-3"> <?php echo $value->id_ticket ?> </td>
                            <td class="p-3"> <?php echo $value->nom ?> </td>
                            <td class="p-3"> <?php echo $value->prenom ?> </td>
                            <td class="p-3"> <?php echo $value->email ?> </td>
                            <td class="p-3"> <?php echo $value->titre ?> </td>
                            <td class="p-3"> <?php echo $value->description ?> </td>
  
                        </tr>
                    <?php endforeach ?>
                </table>
            </div>
        </div>
    </div>
</section>
