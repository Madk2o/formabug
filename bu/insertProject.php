<?php

require "../da/dbaccess.php";

$bdd = bdd();

$sql = "INSERT INTO project (titre, description) VALUES (?, ?)";

$stmt = $bdd->prepare($sql);

$stmt->execute([$_POST["nom"], $_POST["messagejoint"]]);

$id_project = $bdd->lastInsertId();

$sql = "INSERT INTO project_has_users(project_id_project, users_id_users) VALUES (?, ?)";

$stmt = $bdd->prepare($sql);
$stmt->execute([(int)$id_project, (int)$_POST["client"]]);
foreach($_POST["dev"] as $dev){

    $sql = "INSERT INTO project_has_users(project_id_project, users_id_users) VALUES (?, ?)";
    
    $stmt = $bdd->prepare($sql);
    $stmt->execute([(int)$id_project, (int)$dev]);
}
header("location: ../../index.php?pageProject=listProject");
exit;

?>