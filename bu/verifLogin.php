<?php
session_start();
require "../da/login.php";

if (isset($_POST["loginpseudo"]) && isset($_POST["loginpassword"])){

    $options = array(
        'loginpseudo' => FILTER_SANITIZE_STRING,
        'loginpassword' => FILTER_SANITIZE_STRING
    );
    
    $result = filter_input_array(INPUT_POST, $options);
    
    $login = $result["loginpseudo"];
    $mdp = $result["loginpassword"];

    $loging = getLogin($login);
    
    if(password_verify($mdp, $loging->password) && $login === $loging->pseudo){
        $_SESSION["connected"] = true;
        $_SESSION["login_connected"] = $loging->pseudo;
        $_SESSION["id_role_connected"] = $loging->id_role;
        header("location: ../../index.php?page=main&error=false&message=Vous etes maintenant connecté");
        exit();
    }else{
        header("location: ../../index.php?pageLogin=login&error=true&message=mot de passe ou login incorrect");
        exit();
    }
}
?>