<?php

function maxChar($string, $count)
{
    if (strlen($string) > (int)$count) {
        return false;
    }
    return true;
};

function minChar($string, $count)
{
    if (strlen($string) < (int)$count) {
        return false;
    }
    return true;
};
