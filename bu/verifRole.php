<?php
    session_start();

    require "../da/roles.php";
    $pattern ="/^[a-zA-z]*$/";

    //faire les verifications (empty, nbr de caract, ect)

    //une fois les verif faites, on insert en db

    try{
        if(isset($_POST["role"])){

            $role = $_POST["role"];

            if (empty($role)) {  
                $_SESSION["error"] = 1;
                $_SESSION["message"][] = "Erreur, vous n'avez rien rentré.";
            } 
            if (!preg_match($pattern, $role)) {  
                $_SESSION["error"] = 1;
                $_SESSION["message"][] = "Seulement l'alphabet est autorisé.";  
            }
            if(!minChar($role, 3)){
                $_SESSION['error'] = 1;
                $_SESSION['message'][] = "Le role doit avoir plus de 3 lettres";
            }
            if(!maxChar($role, 15)){
                $_SESSION['error'] = 1;
                $_SESSION['message'][] = "Le role ne peu avoir plus de 15 lettres";
            }
            if($_SESSION["error"] === 1 && $_SESSION["id"]){
                header("location: ../index.php?pageRole=modifyRole&id=".$_SESSION["id"]);
                exit();
            }
            if($_SESSION["error"] === 1){
                header("location: ../index.php?pageRole=insertRole");
                exit();
            }
            if(isset($_SESSION["id"])){
                updateRole($role, $_SESSION["id"]);
            }
            
            addRole($role);
        }
    }catch(PDOException $e){

        echo $e->getMessage();
        echo $e->getLine();
        exit;
    }