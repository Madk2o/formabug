<?php 
session_start();
require __DIR__."/../da/users.php";
$patternChar ="/^[a-zA-z]*$/";


try{
    if(isset($_POST["pseudo"]) && isset($_POST["email"]) && isset($_POST["password"])){

        $_SESSION["pseudo"] = $pseudo = $_POST["pseudo"];
        $_SESSION["email"] = $email = $_POST["email"];
        $_SESSION["password"] = $password = $_POST["password"];
        $_SESSION["registerRole"] = $registerRole = $_POST["registerRole"];

        if (empty($pseudo) || empty($email) || empty($password)){  
            $_SESSION["error"] = 1;
            $_SESSION["message"][] = "Erreur, vous n'avez rien rentré.";
        } 
        if (!preg_match($patternChar, $pseudo)) {  
            $_SESSION["error"] = 1;
            $_SESSION["message"][] = "Seulement l'alphabet est autorisé.";  
        }
        if(!minChar($pseudo, 3)){
            $_SESSION['error'] = 1;
            $_SESSION['message'][] = "Le pseudo doit avoir plus de 3 lettres";
        }
        if(!maxChar($pseudo, 15)){
            $_SESSION['error'] = 1;
            $_SESSION['message'][] = "Le pseudo ne peu avoir plus de 15 lettres";
        }
        if($_SESSION["error"] === 1 && $_SESSION["id"]){
            header("location: ../index.php?pageUser=modifyUser&id=".$_SESSION["id"]);
            exit();
        }
        if($_SESSION["error"] === 1){
            header("location: ../index.php?pageUser=createUser");
            exit();
        }
        if(isset($_SESSION["id"])){
            $hash = password_hash($password, PASSWORD_DEFAULT);
            updateUser($pseudo, $email, $hash, $registerRole, $_SESSION["id"]);
        }
        $hash = password_hash($password, PASSWORD_DEFAULT);

        var_dump($hash);
        createUser($pseudo, $email, $hash, $registerRole);
    }
}catch(PDOException $e){

    echo $e->getMessage();
    echo $e->getLine();
    exit;
}



?>