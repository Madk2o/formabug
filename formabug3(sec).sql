-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 30 juin 2021 à 16:23
-- Version du serveur :  10.4.18-MariaDB
-- Version de PHP : 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `formabug2`
--

-- --------------------------------------------------------

--
-- Structure de la table `project`
--

CREATE TABLE `project` (
  `id_project` int(11) NOT NULL,
  `titre` varchar(50) DEFAULT '0',
  `description` varchar(300) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `project`
--

INSERT INTO `project` (`id_project`, `titre`, `description`) VALUES
(3, 'Bistro de thibaut', 'le bistro de THIB'),
(4, 'thibaut le beau', 'issou la thibautiere'),
(34, 'thibaut le king', 'yes le thib'),
(35, 'fsgddbxfd', 'ezrtzert'),
(36, 'Youp', 'crÃ©ation d\'une appli web.');

-- --------------------------------------------------------

--
-- Structure de la table `project_has_users`
--

CREATE TABLE `project_has_users` (
  `project_id_project` int(11) NOT NULL,
  `users_id_users` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `project_has_users`
--

INSERT INTO `project_has_users` (`project_id_project`, `users_id_users`) VALUES
(3, 20),
(3, 23),
(3, 24),
(4, 20),
(4, 23),
(4, 24),
(34, 22),
(34, 24),
(35, 19),
(35, 24),
(36, 19),
(36, 23);

-- --------------------------------------------------------

--
-- Structure de la table `tickets`
--

CREATE TABLE `tickets` (
  `id_ticket` int(11) NOT NULL,
  `nom` varchar(55) CHARACTER SET utf8mb4 DEFAULT NULL,
  `prenom` varchar(55) CHARACTER SET utf8mb4 DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `titre` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tickets`
--

INSERT INTO `tickets` (`id_ticket`, `nom`, `prenom`, `email`, `titre`, `description`) VALUES
(1, 'carbonaro', 'Michele', 'carbonaromichele@live.fr', 'newprojet', 'site de vente'),
(3, 'test', 'teettt', 'tetst@gmail.com', 'titi', '15151'),
(4, NULL, NULL, NULL, NULL, NULL),
(5, NULL, NULL, NULL, NULL, NULL),
(6, NULL, NULL, NULL, NULL, NULL),
(7, NULL, NULL, NULL, NULL, NULL),
(8, NULL, NULL, NULL, NULL, NULL),
(9, NULL, NULL, NULL, NULL, NULL),
(10, NULL, NULL, NULL, NULL, NULL),
(11, NULL, NULL, NULL, NULL, NULL),
(12, NULL, NULL, NULL, NULL, NULL),
(13, NULL, NULL, NULL, NULL, NULL),
(14, NULL, NULL, NULL, NULL, NULL),
(15, NULL, NULL, NULL, NULL, NULL),
(16, NULL, NULL, NULL, NULL, NULL),
(17, NULL, NULL, NULL, NULL, NULL),
(18, NULL, NULL, NULL, NULL, NULL),
(19, NULL, NULL, NULL, NULL, NULL),
(20, NULL, NULL, NULL, NULL, NULL),
(21, NULL, NULL, NULL, NULL, NULL),
(22, NULL, NULL, NULL, NULL, NULL),
(23, NULL, NULL, NULL, NULL, NULL),
(24, NULL, NULL, NULL, NULL, NULL),
(25, NULL, NULL, NULL, NULL, NULL),
(26, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `type_roles`
--

CREATE TABLE `type_roles` (
  `id_role` int(11) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `type_roles`
--

INSERT INTO `type_roles` (`id_role`, `role`) VALUES
(1, 'Admin'),
(2, 'dev'),
(4, 'client');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id_users` int(11) NOT NULL,
  `pseudo` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `id_role` int(11) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id_users`, `pseudo`, `email`, `id_role`, `password`) VALUES
(19, 'jhon', 'johnduff@hotmail.com', 2, '$2y$10$BMeLs2bLnoBwHsKm9YDtiu26MLZCls0iNnnya5AbXQtacm2nT/I62'),
(20, 'jim', 'tak@tik.com', 2, '$2y$10$Gposh1kyAn9GphUFSaB9neuFqQJtur2suMZe9iG8DeMteKFD5Y.Qy'),
(21, 'tok', 'tok@tok.be', 2, '$2y$10$pwCwOCbkVF6lVmKi2smWQOsCsD2x17FoOPrG83cRxjkkgUfYATRre'),
(22, 'madk', 'madk@gmail.be', 1, '$2y$10$PWgX.41c.mLaJETbB1Jq9uujztcrWM0LwZviXHLgjYmFrP8h7ZISi'),
(23, 'tchekkeu', 'tchekeu@chequeuuu.bo', 4, '$2y$10$g8pNtJnX4Dl81fiZs0uDhemfw3dqA0vuHJDGIgS2MIQPQYWYv0ykG'),
(24, 'dav', 'ix@na.org', 4, '$2y$10$/73NnYb2jJySOpYr9xnlD./rDa6aJ9eGPtf9blPOJPh2Wn3BjNTYS'),
(26, 'max', 'max@d.com', 4, '$2y$10$IrbwAINuFaNhEZo5m6fgnuA0ouZ7gAW7mFedA1tySw4YefK96uNvG'),
(28, 'mimi', 'mimi@gmail.com', 2, '$2y$10$zjAgiTdzeVVhWtZtlpiGievXh373l3pFQshM87GeEhSYO.JdNN/m6');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id_project`);

--
-- Index pour la table `project_has_users`
--
ALTER TABLE `project_has_users`
  ADD PRIMARY KEY (`project_id_project`,`users_id_users`),
  ADD KEY `fk_project_has_users_users1_idx` (`users_id_users`),
  ADD KEY `fk_project_has_users_project1_idx` (`project_id_project`);

--
-- Index pour la table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id_ticket`);

--
-- Index pour la table `type_roles`
--
ALTER TABLE `type_roles`
  ADD PRIMARY KEY (`id_role`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_users`),
  ADD KEY `fk_role_id_idx` (`id_role`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `project`
--
ALTER TABLE `project`
  MODIFY `id_project` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT pour la table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id_ticket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT pour la table `type_roles`
--
ALTER TABLE `type_roles`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id_users` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `project_has_users`
--
ALTER TABLE `project_has_users`
  ADD CONSTRAINT `fk_project_has_users_project1` FOREIGN KEY (`project_id_project`) REFERENCES `project` (`id_project`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_project_has_users_users1` FOREIGN KEY (`users_id_users`) REFERENCES `users` (`id_users`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_role_id` FOREIGN KEY (`id_role`) REFERENCES `type_roles` (`id_role`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
