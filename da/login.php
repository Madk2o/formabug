<?php 
    require "dbaccess.php";


function getLogin($login){

    $bdd = bdd();

    $requete = $bdd->prepare("SELECT * FROM users 
                                WHERE pseudo = ?");

    $requete->execute([$login]);
    $result = $requete->fetch(PDO::FETCH_OBJ);

    return $result;
}

?>