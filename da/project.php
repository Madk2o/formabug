<?php
require "dbaccess.php";

function getClientProject(){

    $bdd = bdd();
    $requete = $bdd->prepare("SELECT id_users, pseudo from users
                               WHERE id_role = ?");
    $requete->execute([2]);
    $result = $requete->fetchAll(PDO::FETCH_OBJ);
    return $result;
}
function getDevProject(){

    $bdd = bdd();
    $requete = $bdd->prepare("SELECT id_users, pseudo from users
                               WHERE id_role = ?");
    $requete->execute([4]);
    $result = $requete->fetchAll(PDO::FETCH_OBJ);
    return $result;
}
function createProject(){
    
}
function displayProject(){

    $bdd = bdd();

    try {
        
        $requete = $bdd->prepare("SELECT id_project, titre FROM project");
        $requete->execute();
        $result = $requete->fetchAll(PDO::FETCH_OBJ);
        return $result;

    } catch (PDOException $e) {
        echo $e->getMessage();
        echo $e->getLine();
        exit();
    }
}
function testInnerJoin($user){


    $bdd = bdd();
    $requete = $bdd->prepare("SELECT pj.id_project, pj.titre, pj.description, u.pseudo, ph.users_id_users  
                            FROM project pj 
                            INNER JOIN project_has_users ph on pj.id_project = ph.project_id_project 
                            INNER JOIN users u on ph.users_id_users = u.id_users 
                            WHERE u.pseudo = ?
                            ORDER BY pj.titre");

    $requete->execute([$user]);
    $result = $requete->fetchAll(PDO::FETCH_OBJ);
    return $result;
}

function deleteProject($id){
    $bdd = bdd();

    $stmt2 = $bdd->prepare("DELETE FROM project_has_users WHERE project_id_project = ?");
    $stmt2->execute([$id]);

    $stmt1 = $bdd->prepare("DELETE FROM project WHERE id_project = ?");
    $stmt1->execute([$id]);
    
}