<?php

require "dbaccess.php";


function addRole($role)
{
    $bdd = bdd();
    try {
        $sql = "INSERT INTO type_roles(role)
                VALUES (?)";

        $stmt = $bdd->prepare($sql);
        $stmt->execute([$role]) or die(var_dump($stmt->errorInfo(), TRUE));
        $_SESSION["role"] = null;
        $_SESSION["id"] = null;
        header('location: ../index.php?pageRole=listRole');
    } catch (PDOException $e) {
        echo $e->getMessage();
        echo $e->getLine();
        exit();
    }
};

function displayRole(){

    $bdd = bdd();

    try {
        
        $requete = $bdd->prepare("SELECT * FROM type_roles");
        $requete->execute();
        $result = $requete->fetchAll(PDO::FETCH_OBJ);
        return $result;

    } catch (PDOException $e) {
        echo $e->getMessage();
        echo $e->getLine();
        exit();
    }
}

function updateRole($role, $id)
{
    $bdd = bdd();

    try {
        $requete = $bdd->prepare("UPDATE type_roles SET role = ? 
        WHERE id_role = ?");

        $requete->execute([$role, $id]) or die(var_dump($requete->errorInfo(), TRUE));

        $_SESSION["role"] = null;
        $_SESSION["id"] = null;

        header("location: ../index.php?pageRole=listRole");
        exit();
    } catch (PDOException $e) {
        console_log($e);
        echo $e->getMessage();
        echo $e->getLine();
        exit();
    }
}

function editRole($id){
    try{
        $bdd = bdd();
        
        $requete = $bdd->prepare("SELECT * FROM type_roles WHERE id_role = ?");
        $requete->execute([$id]);
        
        $result = $requete->fetch(PDO::FETCH_OBJ);
        
        return $result;
        
    }catch(PDOException $e){
        
        echo $e->getMessage();
        echo $e->getLine();
        exit;
    }
};

function console_log($data){
    echo '<script>';
    echo 'console.log('. json_encode( $data ) .')';
    echo '</script>';
  }

function maxChar($string, $count){

    if(strlen($string) > (int)$count){
        return false;
    }
    return true;
}

function minChar($string, $count){
    if(strlen($string) < (int)$count){
        return false;
    }
    return true;
}