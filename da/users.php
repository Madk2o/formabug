<?php

require __DIR__."/dbaccess.php";

function maxChar($string, $count){

    if(strlen($string) > (int)$count){
        return false;
    }
    return true;
}

function minChar($string, $count){
    if(strlen($string) < (int)$count){
        return false;
    }
    return true;
}

function createUser($pseudo, $email, $password, $registerRole){
    $bdd = bdd();
    try {
        $sql = "INSERT INTO users(pseudo, email, password, id_role )
                VALUES (?, ?, ?, ?)";

        $stmt = $bdd->prepare($sql);
        $stmt->execute([$pseudo, $email, $password, $registerRole]) or die(var_dump($stmt->errorInfo(), TRUE));

        $_SESSION["pseudo"] = null;
        $_SESSION["email"] = null;
        $_SESSION["password"] = null;
        $_SESSION["registerRole"] = null;

        header('location: ../index.php?pageUser=listUser');
        exit();
    } catch (PDOException $e) {
        echo $e->getMessage();
        echo $e->getLine();
        exit();
    }
}

function displayUser(){

    $bdd = bdd();

    try {
        
        $requete = $bdd->prepare("SELECT id_users, pseudo, email, id_role  FROM users");
        $requete->execute();
        $result = $requete->fetchAll(PDO::FETCH_OBJ);
        return $result;

    } catch (PDOException $e) {
        echo $e->getMessage();
        echo $e->getLine();
        exit();
    }
}

function updateUser($pseudo, $email, $password, $id_role, $id_user)
{
    $bdd = bdd();

    try {
        $requete = $bdd->prepare("UPDATE users SET pseudo = ?, email = ?, password = ?, id_role = ? 
        WHERE id_users = ?");

        $requete->execute([$pseudo, $email, $password, $id_role, $id_user]) or die(var_dump($requete->errorInfo(), TRUE));

        $_SESSION["pseudo"] = null;
        $_SESSION["password"] = null;
        $_SESSION["id"] = null;
        $_SESSION["email"] = null;
        $_SESSION["id_role"] = null;

        header("location: ../index.php?pageUser=listUser");
        exit();
    } catch (PDOException $e) {
        console_log($e);
        echo $e->getMessage();
        echo $e->getLine();
        exit();
    }
}

function editUser($id){

    $bdd = bdd();
        
        $requete = $bdd->prepare("SELECT * FROM users WHERE id_users = ?");
        $requete->execute([$id]);
        
        $result = $requete->fetch(PDO::FETCH_OBJ);
        
        return $result;
}